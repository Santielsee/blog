<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    //
    public function index(){

        return view('register');
    }

    public function welcome(Request $request){

        $first_name = $request->input('nama_depan');
        $last_name = $request->input('nama_belakang');

        return view('welcomeback' , compact('first_name', 'last_name'));
    }
}
