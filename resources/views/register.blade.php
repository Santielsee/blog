<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Form Sign Up</title>
</head>
<body>
    
    <h1>Buat Account Baru!</h1>

    <h4>Sign Up Form</h4>

    <div>
        <form action="/register/selamat" method="POST">
            <!--First name and Last name (Using Input fields and lable only)-->
            <input type = "hidden" name = "_token" value = "<?php echo csrf_token() ?>">
            <div>
                <label for="fname">First name    : </label> <br><br>
            
                <input type="text" id="fname" name="nama_depan" value=""> <br> <br>
            
                <label for="lastname">Last name :</label> <br><br>
           
                <input type="text" id="lastname" name="nama_belakang" value="">
            </div>

            <br>

            <!--Gender (Using radio button)-->
            <div>
                
                <div>Gender :</div> <br>
                <div>
                    <input type="radio" id="male" name="gender" value="male">
                    <label for="male">Male</label> <br>
                    <input type="radio" id="female" name="gender" value="female">
                    <label for="female">Female</label> <br>
                    <input type="radio" id="other" name="gender" value="other">
                    <label for="other">Other</label>
                </div>
        
            </div>

            <br>

            <!--Nationality (Using select-option)-->
            <div>

                <div>Nationality :</div> <br>
                <div>
                    <select name="nationality" id="nationality">
                        <option value="Indonesian">Indonesian</option>
                        <option value="Malaysian">Malaysian</option>
                        <option value="Indian">Indian</option>
                        <option value="Afghani">Afghani</option>
                    </select>
                </div>
                
            </div>

            <br>

            <!--Language Spoken (Using Checkbox)-->
            <div>
                    
                <div>Language Spoken :</div> <br>
                <div>
                    <input type="checkbox" name="lang1" id="bhs_ind">
                    <label for="bhs_ind">Bahasa Indonesia</label> <br>
                    <input type="checkbox" name="lang2" id="english">
                    <label for="english">English</label> <br>
                    <input type="checkbox" name="lang3" id="other">
                    <label for="other">Other</label>
                </div>
                
            </div>

            <br>

            <!--Bio (Using Text-area)-->
            <div>

                <div>Bio    : </div> <br>
                <div>
                    <textarea name="bio" id="" cols="30" rows="10"></textarea> <br>
                    <input type="submit" name="" id="" value="Sign Up">
                </div>

            </div>
            
        </form>
    </div>
        
    
</body>
</html>

